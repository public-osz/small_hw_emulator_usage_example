#include <iostream>
#include <chrono>
#include <thread>

#include "small_hw_emulator_lib.h"

int main(int argc, char *argv[])
{
    using namespace std::chrono_literals;

    std::cout << "init: " << emulator_lib::init("", "") << "!\n";

    emulator_lib::setLCD("testAABB133434341122333abcde12345");
    
    std::cout << "getV1: " << emulator_lib::getVoltage1() << "\n";
    std::cout << "getV2: " << emulator_lib::getVoltage2() << "\n";

    std::cout << "getIn1: " << emulator_lib::getInput1() << "\n";
    std::cout << "getIn2: " << emulator_lib::getInput2() << "\n";
    std::cout << "getIn3: " << emulator_lib::getInput3() << "\n";

    emulator_lib::setLed1(true);
    emulator_lib::setLed2(true);
    emulator_lib::setLed3(true);

    std::this_thread::sleep_for(1000ms);

    emulator_lib::setLCD("zxcvbnm");
    
    emulator_lib::setLed1(false);
    emulator_lib::setLed2(true);
    emulator_lib::setLed3(false);

    return 0;
}
