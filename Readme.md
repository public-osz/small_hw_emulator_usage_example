

Install:
```
apt install cmake
sudo apt install build-essential
```

Build:
```
mkdir build
cd build
cmake ..
make
```

Run:
```
./small_he_emulator_usage_example
```
